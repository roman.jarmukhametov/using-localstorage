/**
 * Email Validator Module
 *
 * This module provides functionality to validate email addresses based on their domain endings.
 * It checks if the email address belongs to one of the predefined valid domains.
 */

// Array of valid email domains.
const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

/**
 * Validates an email address against a list of valid domains.
 *
 * This function extracts the domain part of the email address and checks if it matches
 * any of the domains specified in the VALID_EMAIL_ENDINGS array.
 *
 * @param {string} email - The email address to validate.
 * @returns {boolean} True if the email's domain is in the list of valid domains, false otherwise.
 */
function validate(email) {
  // Extract the domain from the email address by splitting it at the "@" symbol and taking the last part.
  const emailDomain = email.substring(email.lastIndexOf("@") + 1);

  // Check if the extracted domain is in the list of valid email endings.
  return VALID_EMAIL_ENDINGS.includes(emailDomain);
}

// Export the validate function to allow its use in other modules.
export { validate };
