/**
 * Main entry point for the application.
 *
 * This script is responsible for importing necessary stylesheets and initializing
 * specific modules when the DOM is fully loaded. It ensures that the application's
 * UI components are correctly set up and displayed to the user.
 */

// Imports the main stylesheet for the application.
// This import statement allows Webpack (or a similar bundler) to process and include
// the stylesheet in the build output.
import "../styles/style.css";

// Imports the JoinUsSection module from the join-us-section.js file.
// This module is responsible for creating and displaying the "Join Our Program"
// section of the application, including the subscription form.
import { JoinUsSection } from "./join-us-section.js";

// Adds an event listener for the 'DOMContentLoaded' event to ensure that the
// JoinUsSection module is only initialized once the DOM is fully loaded.
// This prevents any attempts to manipulate DOM elements before they are available.
document.addEventListener("DOMContentLoaded", function () {
  // Calls the init method of the JoinUsSection module to create and insert
  // the "Join Our Program" section into the DOM. This setup step is crucial
  // for displaying the subscription section to the user.
  JoinUsSection.init();
});
